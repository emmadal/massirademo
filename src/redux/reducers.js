const defaultState = {
  fetching: false,
  authenticated: false,
  user: {},
  messages: [],
};

export const AuthReducer = (state = defaultState, action) => {
  switch (action.type) {
    case 'AUTH':
      return Object.assign({}, state, {
        fetching: true,
        user: action.user,
        messages: [...action.messages],
      });
    case 'SEND_MESSAGE':
      return Object.assign({}, state, {
        messages: [...state.messages, ...action.message],
      });
    default:
      return state;
  }
};
