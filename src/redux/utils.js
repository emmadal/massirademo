import {sendMessageAction, authAction} from './actions';

export const mapStateToProps = state => {
  return {
    data: state,
  };
};

export const mapDispatchToProps = dispatch => {
  return {
    authUser: username => dispatch(authAction(username)),
    sendMessage: text => dispatch(sendMessageAction(text)),
  };
};
