const AUTH = 'AUTH';
const SEND_MESSAGE = 'SEND_MESSAGE';

const authAction = username => {
  return {
    type: AUTH,
    users: username.data,
    message: [...username.data.messages],
  };
};

const sendMessageAction = message => {
  return {
    type: SEND_MESSAGE,
    message,
  };
};

export {authAction, sendMessageAction};
