import React from 'react';
import {StyleSheet, Modal, ActivityIndicator, View} from 'react-native';
import {useTheme} from 'react-native-paper';

export const Loader = ({isLoading}) => {
  const {colors} = useTheme();
  return (
    <Modal visible={isLoading} transparent={true} animationType="fade">
      <View style={styles.modalContainer}>
        <View style={styles.spinnerBox}>
          <ActivityIndicator
            animating={isLoading}
            size={60}
            color={colors.primary}
          />
        </View>
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modalContainer: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040',
  },
  spinnerBox: {
    backgroundColor: '#FFFFFF',
    height: 70,
    width: 70,
    borderRadius: 10,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
});
