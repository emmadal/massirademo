import React from 'react';
import {useNavigation} from '@react-navigation/native';
import {TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import {useTheme} from 'react-native-paper';

export const LogoutBox = () => {
  const navigation = useNavigation();
  const {colors} = useTheme();
  return (
    <TouchableOpacity onPress={() => navigation.goBack()}>
      <Icon name="power-sharp" size={30} color={colors.primary} />
    </TouchableOpacity>
  );
};
