import React, {useState} from 'react';
import {View, StyleSheet, Image, SafeAreaView} from 'react-native';
import {Button, TextInput} from 'react-native-paper';
import {useNavigation} from '@react-navigation/native';
import {Loader} from '../components/Loader';

export const LoginScreen = ({authUser}) => {
  const [userName, setUserName] = useState('');
  const [loading, setLoading] = useState(false)
  const navigation = useNavigation();

  const handleChange = e => setUserName(e);
  const onLogin = () => {
    setLoading(!loading);
    // navigation.navigate('chatroom');
  };

  return (
    <>
      <Loader isLoading={loading} />
      <View style={styles.container}>
        <Image source={require('../assets/icon.png')} style={styles.logo} />
        <TextInput
          mode="outlined"
          label="Name"
          name="userName"
          value={userName}
          onChangeText={text => handleChange(text)}
          placeholder="Enter your name"
          activeOutlineColor="#FF0A45A1"
        />
        <Button
          color="#FF0A45A1"
          compact
          labelStyle={styles.labelStyle}
          mode="contained"
          style={styles.btn}
          onPress={onLogin}>
          Login
        </Button>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 30,
  },
  btn: {
    marginTop: 30,
    padding: 5,
    borderRadius: 30,
  },
  labelStyle: {
    color: '#fff',
    fontWeight: 'bold',
  },
  logo: {
    alignSelf: 'center',
  },
});
