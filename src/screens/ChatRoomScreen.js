import React, {useState, useCallback, useEffect} from 'react';
import {Text, ImageBackground, StyleSheet, ScrollView} from 'react-native';
import {GiftedChat} from 'react-native-gifted-chat';

export const ChatRoomScreen = () => {
  const [messages, setMessages] = useState([]);

  useEffect(() => {
    setMessages([
      {
        _id: 1,
        text: 'Hello developer',
        createdAt: new Date(),
        user: {
          _id: 2,
          name: 'React Native',
          avatar: 'https://placeimg.com/140/140/any',
        },
      },
    ]);
  }, []);

  const onSend = useCallback((messages = []) => {
    setMessages(previousMessages =>
      GiftedChat.append(previousMessages, messages),
    );
  }, []);
  return (
    <ImageBackground
      source={require('../assets/backgroundchat.jpeg')}
      style={styles.container}
      resizeMode="cover">
      <GiftedChat
        scrollToBottom={true}
        messages={messages}
        onSend={message => onSend(message)}
        user={{
          _id: 1,
        }}
      />
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
