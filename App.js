/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {DefaultTheme, Provider as PaperProvider} from 'react-native-paper';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import thunk from 'redux-thunk';
import {Provider, connect} from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import {LoginScreen} from './src/screens/LoginScreen';
import {ChatRoomScreen} from './src/screens/ChatRoomScreen';
import {LogoutBox} from './src/components/LogoutBox';
import {AuthReducer} from './src/redux/reducers';
import {mapStateToProps, mapDispatchToProps} from './src/redux/utils';

const theme = {
  ...DefaultTheme,
  roundness: 2,
  colors: {
    ...DefaultTheme.colors,
    primary: '#FF0A45A1',
    accent: '#f1c40f',
  },
};

// Create Stack navigator for the screen navigation
const Stack = createNativeStackNavigator();

// Create redux store
const store = createStore(AuthReducer, applyMiddleware(thunk));

const Root = () => {
  return (
    <PaperProvider theme={theme}>
      <NavigationContainer>
        <Stack.Navigator>
          <Stack.Screen
            name="Login"
            component={LoginScreen}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="chatroom"
            component={ChatRoomScreen}
            options={() => ({
              headerTitle: 'Chat Room',
              headerRight: () => <LogoutBox />,
            })}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </PaperProvider>
  );
};
connect(mapStateToProps, mapDispatchToProps)(Root);

const App = () => {
  return (
    <Provider store={store}>
      <Root />
    </Provider>
  );
};

export default App;
